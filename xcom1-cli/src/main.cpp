#include <iostream>

#include "game_data.hpp"


const std::string GAME_BASE_DIR = "C:/Program Files (x86)/Steam/steamapps/common/XCom UFO Defense/XCOM";
const std::string GAME1_DIR = GAME_BASE_DIR + "/GAME_1";

int main(int, char*[]) {
    GameData game1Data(GAME1_DIR);
    Liglob liglobObj = game1Data.getLiglob();
    std::cout << "Current money: " << liglobObj.money << std::endl;

    liglobObj.money += 10000000;
    std::cout << "New money:" << liglobObj.money << std::endl;

    game1Data.writeLiglob(liglobObj);

    liglobObj.money = 0;

    liglobObj = game1Data.getLiglob();

    std::cout << "Read saved liglob. Money: " << liglobObj.money << std::endl;

    return 0;
}
