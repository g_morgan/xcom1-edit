#ifndef LIGLOB_HPP
#define LIGLOB_HPP

#include <istream>
#include <ostream>
#include <array>

#include "data_types.hpp"

class Liglob {
    static const size_t LIGLOB_BLOCK1_SIZE = 144;
public:
    lei32 money;
    std::array<unsigned char, LIGLOB_BLOCK1_SIZE> data;

    void readFromStream(std::istream &inputStream);
    void writeToStream(std::ostream &outputStream);
};

Liglob getLiglobFromFilename(std::string liglobFilename);
void writeLiglobToFilename(std::string liglobFilename, Liglob& liglob);

#endif
