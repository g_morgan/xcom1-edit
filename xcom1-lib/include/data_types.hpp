#ifndef DATA_TYPES_HPP
#define DATA_TYPES_HPP

#include <cstdint>
#include <istream>
#include <ostream>
#include <array>

template <class T>
class lei {
public:
    static const T LOW_BYTE_MASK = 0xFF;
    T value;

    lei(T newValue);
    lei() : lei(0) {};
    lei(const lei<T> &copyValue) : lei(copyValue.value) {};

    void readFromStream(std::istream &inputStream);
    void writeToStream(std::ostream &outputStream);

    template<class U> friend bool operator== (const lei<U> &lhs, const lei<U> &rhs);
    template<class U> friend bool operator== (const lei<U> &lhs, const U rhs);
    template<class U> friend bool operator== (const U lhs, const lei<U> &rhs);
    template<class U> friend std::ostream& operator<< (std::ostream& os, const lei<U>& dt);

    lei<T>& operator+=(const lei<T>& rhs);
    lei<T>& operator+=(const T rhs);
};

template <class T>
lei<T>::lei(T newValue) {
    this->value = newValue;
}

template <class T>
void lei<T>::readFromStream(std::istream &inputStream) {
    std::array<unsigned char, sizeof(T)> data;
    inputStream.read((char *) data.data(), data.size());

    T tmpValue = 0;
    for(int i = data.size() - 1; i >= 0; --i) {
        tmpValue = tmpValue << 8;
        tmpValue |= data[i];
    }

    this->value = tmpValue;
}

template <class T>
void lei<T>::writeToStream(std::ostream &outputStream) {
    T tmpValue = this->value;
    std::array<unsigned char, sizeof(T)> data;

    for(unsigned char& curByte : data) {
        curByte = (unsigned char) lei<T>::LOW_BYTE_MASK & tmpValue;
        tmpValue = tmpValue >> 8;
    }
    outputStream.write((char *) data.data(), data.size());
}

template <class T>
bool operator== (const lei<T> &lhs, const lei<T> &rhs)
{
    return lhs.value == rhs.value;
}

template <class T>
bool operator== (const lei<T> &lhs, const T rhs)
{
    return lhs.value == rhs;
}

template <class T>
bool operator== (const T lhs, const lei<T> &rhs)
{
    return lhs == rhs.value;
}

template <class T>
std::ostream& operator<< (std::ostream& os, const lei<T>& leObj)
{
    os << leObj.value;
    return os;
}

template <class T>
lei<T>& lei<T>::operator+=(const lei<T>& rhs){

      this->value += rhs.value;
      return *this;
}

template <class T>
lei<T>& lei<T>::operator+=(const T rhs){

      this->value += rhs;
      return *this;
}

typedef lei<int32_t> lei32;
typedef lei<int16_t> lei16;

#endif
