#ifndef GAME_DATA_HPP
#define GAME_DATA_HPP

#include <string>
#include <exception>

#include "liglob.hpp"

bool isValidSaveGameDirectory(std::string saveGameDir);

class InvalidSaveGameDirectoryException : public std::exception {
public:
    static const std::string INVALID_GAME_DIR_MSG;
    const std::string saveDir;

    InvalidSaveGameDirectoryException(const std::string& saveDir) : saveDir(saveDir) {}

    const char* what() const noexcept {
        return (INVALID_GAME_DIR_MSG + this->saveDir).c_str();
    };
};

class GameData {
private:
    GameData();
public:
    const std::string saveDir;

    GameData(const std::string& saveDir);

    std::string getLiglobFilename();
    Liglob getLiglob();
    void writeLiglob(Liglob& liglob);
};

#endif
