#ifndef BASE_HPP
#define BASE_HPP

#include <istream>
#include <ostream>
#include <array>
#include <exception>

#include "data_types.hpp"

enum BaseDataError {
    UNTERMINATED_BASE_NAME = 1
};

class InvalidBaseDataException : public std::exception {
public:
    static const std::string UNTERMINATED_BASE_NAME_MSG;
    InvalidBaseDataException(std::string errMessage, BaseDataError errType) : InvalidBaseDataException(errMessage.c_str(), errType) {};
    InvalidBaseDataException(const char* errMessage, BaseDataError errType) : errType(errType), errMessage(errMessage) {};

    const char* what() const noexcept;
    const BaseDataError errType;
private:
    const char* errMessage;
};

class BaseData {
    static const size_t BASE_NAME_SIZE = 14;
    static const size_t BASE_DATA_BLOCK1_SIZE = 80;
public:
    std::array<char, BASE_NAME_SIZE> baseName;
    std::array<unsigned char, BASE_DATA_BLOCK1_SIZE> block1;
    uint8_t numberEngineers;
    uint8_t numberScientists;

    lei16 numberStingrayLauncher;
    lei16 numberAvalancheLauncher;
    lei16 numberCannon;
    lei16 numberFusionBallLauncher;
    lei16 numberLaserCannon;
    lei16 numberPlasmaBeam;
    lei16 numberStingrayMissile;
    lei16 numberAvalancheMissile;
    lei16 numberCannonRounds;
    lei16 numberFusionBall;

    lei16 numberCannonTank;
    lei16 numberRocketTank;
    lei16 numberLaserTank;
    lei16 numberPlasmaHoverTank;
    lei16 numberLauncherHoverTank;

    lei16 numberPistol;
    lei16 numberPistolClip;
    lei16 numberRifle;
    lei16 numberRifleClip;
    lei16 numberHeavyCannon;
    lei16 numberHCArmourPiercing;
    lei16 numberHCHighExplosive;
    lei16 numberHCIncendiary;
    lei16 numberAutoCanon;
    lei16 numberACArmourPiercing;
    lei16 numberACHighExplosive;
    lei16 numberACIncendiary;
    lei16 numberRocketLauncher;
    lei16 numberSmallRocket;
    lei16 numberLargeRocket;
    lei16 numberIncendiaryRocket;
    lei16 numberLaserPistol;
    lei16 numberLaserRifle;
    lei16 numberHeavyLaser;
    lei16 numberGrenade;
    lei16 numberSmokeGrenade;
    lei16 numberProximityGrenade;
    lei16 numberHighExplosive;
    lei16 numberMotionScanner;
    lei16 numberMediKit;
    lei16 numberPsiAmp;
    lei16 numberStunRod;
    lei16 numberElectroFlare;

    lei16 empty1;
    lei16 empty2;
    lei16 empty3;
    lei16 empty4;
    lei16 empty5;
    lei16 empty6;

    lei16 numberHeavyPlasma;
    lei16 numberHeavyPlasmaClip;
    lei16 numberPlasmaRifle;
    lei16 numberPlasmaRifleClip;
    lei16 numberPlasmaPistol;
    lei16 numberPlasmaPistolClip;
    lei16 numberBlasterLauncher;
    lei16 numberBlasterBomb;
    lei16 numberSmallLauncher;
    lei16 numberStunBomb;
    lei16 numberAlienGrenade;
    lei16 numberElerium115;
    lei16 numberMindProbe;

    lei16 empty7;
    lei16 empty8;
    lei16 empty9;

    lei16 numberSectoidCorpse;
    lei16 numberSnakemanCorpse;
    lei16 numberEtherealCorpse;
    lei16 numberMutonCorpse;
    lei16 numberFloaterCorpse;
    lei16 numberCelatidCorpse;
    lei16 numberSilacoidCorpse;
    lei16 numberChryssalidCorpse;
    lei16 numberReaperCorpse;
    lei16 numberSectopodCorpse;
    lei16 numberCyberdiscCorpse;

    lei16 empty10;
    lei16 empty11;
    lei16 empty12;
    lei16 empty13;

    lei16 numberUfoPowerSource;
    lei16 numberUfoNavigation;
    lei16 numberUfoConstruction;
    lei16 numberAlienFood;
    lei16 numberAlienReproduction;
    lei16 numberAlienEntertainment;
    lei16 numberAlienSurgery;
    lei16 numberExaminationRoom;
    lei16 numberAlienAlloys;
    lei16 numberAlienHabitat;

    lei16 numberPersonalArmour;
    lei16 numberPowerSuit;
    lei16 numberFlyingSuit;

    lei16 numberHwpCannonShell;
    lei16 numberHwpRocket;
    lei16 numberHwpFusionBomb;

    lei32 baseActive;

    void readFromStream(std::istream& inputStream);
    void writeToStream(std::ostream& outputStream);

    bool isBaseNameValid();
    std::string getBaseName();

    bool isBaseActive();
    void setBaseActive(bool activeValue);
};

class Base {
    static const int NO_BASES = 8;
public:
    std::array<BaseData, NO_BASES> bases;

    void readFromStream(std::istream& inputStream);
    void writeToStream(std::ostream& outputStream);
};

#endif
