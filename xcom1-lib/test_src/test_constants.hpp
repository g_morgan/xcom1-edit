#ifndef TEST_CONSTANTS_HPP
#define TEST_CONSTANTS_HPP

#include <string>

#ifndef XCOM_LIB_DIR_DEF
#define XCOM_LIB_DIR_DEF "C:/Users/Gareth/Documents/projects/xcom1-edit/xcom1-lib"
#endif

//General Constants
extern const std::string XCOM_LIB_DIR;
extern const std::string TEST_DATA_DIR;
extern const std::string TMP_TEST_DATA_DIR;

//Example Game Constants
extern const std::string GAME_0001_DIR;
extern const std::string GAME_0001_LIGLOB_FILE;
extern const std::string GAME_0001_BASE_FILE;
extern const std::string GAME_0001_BASE1_BASENAME;

//test_data_types constants
extern const std::string TEST_DATA_TYPES_DIR;
extern const std::string I32_4153000_FILE;
extern const std::string TEST1_1_I32_4153000_TMP_FILE;
extern const std::string I32_N4153000_FILE;
extern const std::string TEST2_1_I32_N4153000_TMP_FILE;
extern const std::string I16_22434_FILE;
extern const std::string TEST3_1_I16_22434_TMP_FILE;
extern const std::string I16_N22434_FILE;
extern const std::string TEST4_1_I16_N22434_TMP_FILE;

//test_liglob constants
extern const std::string TMP_TEST1_1_LIGLOB_FILE;
extern const std::string TEST1_2_LIGLOB_FILE;
extern const std::string TMP_TEST1_2_LIGLOB_FILE;

//test_base constants
extern const std::string TEST_BASE_DATA_DIR;
extern const std::string TMP_TEST1_1_BASE_FILE;
extern const std::string T2_INVALID_BASENAME_BASE_FILE;

//test_game_data constants
extern const std::string NOT_REAL_PATH;
extern const std::string TEST_GAME_DATA_TMP_DIR;
extern const std::string TEST_GAME_DATA_T5_1_TMP_LIGLOB;
#endif
