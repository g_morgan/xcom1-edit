#include <catch.hpp>

#include "game_data.hpp"
#include "test_constants.hpp"
#include "test_utils.hpp"

TEST_CASE( "Test 1. Show GAME_0001 is a valid save directory" ) {
    REQUIRE(isValidSaveGameDirectory(GAME_0001_DIR));
    REQUIRE_NOTHROW( GameData(GAME_0001_DIR) );
}

TEST_CASE( "Test 2. Show non-existent path is not a valid save directory" ) {
    REQUIRE(!isValidSaveGameDirectory(NOT_REAL_PATH));
    REQUIRE_THROWS_AS( GameData(NOT_REAL_PATH), InvalidSaveGameDirectoryException );
}

TEST_CASE( "Test 3. Show GAME_0001 liglob file is not a valid save directory" ) {
    REQUIRE(!isValidSaveGameDirectory(GAME_0001_LIGLOB_FILE));
    REQUIRE_THROWS_AS( GameData(GAME_0001_LIGLOB_FILE), InvalidSaveGameDirectoryException );
}

TEST_CASE( "Test 4. Show test_base directory is not a valid save directory" ) {
    REQUIRE(!isValidSaveGameDirectory(TEST_BASE_DATA_DIR));
    REQUIRE_THROWS_AS( GameData(TEST_BASE_DATA_DIR), InvalidSaveGameDirectoryException );
}

TEST_CASE( "Test 5. Read values from GAME_0001") {
    GameData game0001(GAME_0001_DIR);
    Liglob game0001Liglob = game0001.getLiglob();
    REQUIRE( game0001Liglob.money == 4153000 );

    SECTION( "1 write game data unchanged" ) {
        createDirIfDoesntExist(TEST_GAME_DATA_TMP_DIR);
        writeLiglobToFilename(TEST_GAME_DATA_T5_1_TMP_LIGLOB, game0001Liglob);
        compareFiles(game0001.getLiglobFilename(), TEST_GAME_DATA_T5_1_TMP_LIGLOB);
    }
}
