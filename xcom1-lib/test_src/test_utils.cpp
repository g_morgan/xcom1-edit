#include "test_utils.hpp"

#include <catch.hpp>
#include <fstream>
#include <boost/filesystem.hpp>

void readFileToVec(const std::string& filename, std::vector<char>& vec) {
	std::ifstream f(filename.c_str());
	REQUIRE( f.good() );
	REQUIRE( f.is_open() );
	REQUIRE( f.peek() != EOF);
	while(f.peek() != EOF) {
		char curByte;
		f.get(curByte);
		vec.push_back(curByte);
	}
}

void compareFiles(const std::string& filename1, const std::string& filename2) {
	std::vector<char> vec1;
	readFileToVec(filename1, vec1);

	std::vector<char> vec2;
	readFileToVec(filename2, vec2);

	REQUIRE( vec1 == vec2 );
}


void createDirIfDoesntExist(const std::string& dirname) {
    boost::filesystem::path dirPath(dirname);
    if(!boost::filesystem::exists(dirPath)) {
        boost::filesystem::create_directory(dirPath);
    }
}
