#ifndef TEST_UTILS_HPP
#define TEST_UTILS_HPP

#include <string>

void compareFiles(const std::string& filename1, const std::string& filename2);
void createDirIfDoesntExist(const std::string& dirname);

#endif
