#include <catch.hpp>
#include <iostream>
#include <fstream>

#include "data_types.hpp"
#include "test_utils.hpp"
#include "test_constants.hpp"

TEST_CASE( "Test 1. Read i32-4153000 integer" ) {
    lei32 data;
    {
        std::ifstream i32_4153000(I32_4153000_FILE.c_str());
        REQUIRE( i32_4153000.is_open() );
        data.readFromStream(i32_4153000);
        REQUIRE( data.value == 4153000 );
    }

    SECTION( "1 write data unchanged" ) {
        {
            std::ofstream test1_1_output(TEST1_1_I32_4153000_TMP_FILE.c_str());
            data.writeToStream(test1_1_output);
        }
        compareFiles(I32_4153000_FILE, TEST1_1_I32_4153000_TMP_FILE);
    }
}

TEST_CASE( "Test 2. Read i32-N4153000 integer" ) {
    lei32 data;
    {
        std::ifstream i32_n4153000(I32_N4153000_FILE.c_str());
        REQUIRE( i32_n4153000.is_open() );
        data.readFromStream(i32_n4153000);
        REQUIRE( data.value == -4153000 );
    }

    SECTION( "1 write data unchanged" ) {
        {
            std::ofstream test2_1_output(TEST2_1_I32_N4153000_TMP_FILE.c_str());
            data.writeToStream(test2_1_output);
        }
        compareFiles(I32_N4153000_FILE, TEST2_1_I32_N4153000_TMP_FILE);
    }
}

TEST_CASE( "Test 3. Read i16-22434 integer" ) {
    lei16 data;
    {
        std::ifstream i16_22434(I16_22434_FILE.c_str());
        REQUIRE( i16_22434.is_open() );
        data.readFromStream(i16_22434);
        REQUIRE( data.value == 22434 );
    }

    SECTION( "1 write data unchanged" ) {
        {
            std::ofstream test3_1_output(TEST3_1_I16_22434_TMP_FILE.c_str());
            data.writeToStream(test3_1_output);
        }
        compareFiles(I16_22434_FILE, TEST3_1_I16_22434_TMP_FILE);
    }
}

TEST_CASE( "Test 4. Read i16-N22434 integer" ) {
    lei16 data;
    {
        std::ifstream i16_N22434(I16_N22434_FILE.c_str());
        REQUIRE( i16_N22434.is_open() );
        data.readFromStream(i16_N22434);
        REQUIRE( data.value == -22434 );
    }

    SECTION( "1 write data unchanged" ) {
        {
            std::ofstream test4_1_output(TEST4_1_I16_N22434_TMP_FILE.c_str());
            data.writeToStream(test4_1_output);
        }
        compareFiles(I16_N22434_FILE, TEST4_1_I16_N22434_TMP_FILE);
    }
}
