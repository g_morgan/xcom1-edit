#include <catch.hpp>
#include <fstream>

#include "base.hpp"
#include "test_constants.hpp"
#include "test_utils.hpp"

TEST_CASE( "Test 1. Read GAME_0001" ) {
    Base baseObj;
    {
        std::ifstream game001Base(GAME_0001_BASE_FILE.c_str());
        REQUIRE( game001Base.is_open() );
        baseObj.readFromStream(game001Base);
        REQUIRE( game001Base.peek() == EOF );

        REQUIRE( baseObj.bases[0].getBaseName() == GAME_0001_BASE1_BASENAME );
        REQUIRE( baseObj.bases[0].numberEngineers == 10 );
        REQUIRE( baseObj.bases[0].numberScientists == 10 );
        REQUIRE( baseObj.bases[0].numberStingrayLauncher == (int16_t) 1);
        REQUIRE( baseObj.bases[0].numberAvalancheLauncher == (int16_t) 1);
        REQUIRE( baseObj.bases[0].numberCannon == (int16_t) 2);
        REQUIRE( baseObj.bases[0].numberFusionBallLauncher == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberLaserCannon == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberPlasmaBeam == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberStingrayMissile == (int16_t) 25);
        REQUIRE( baseObj.bases[0].numberAvalancheMissile == (int16_t) 10);
        REQUIRE( baseObj.bases[0].numberCannonRounds == (int16_t) 1);
        REQUIRE( baseObj.bases[0].numberFusionBall == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberCannonTank == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberRocketTank == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberLaserTank == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberPlasmaHoverTank == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberLauncherHoverTank == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberPistol == (int16_t) 2);
        REQUIRE( baseObj.bases[0].numberPistolClip == (int16_t) 8);
        REQUIRE( baseObj.bases[0].numberRifle == (int16_t) 2);
        REQUIRE( baseObj.bases[0].numberRifleClip == (int16_t) 8);
        REQUIRE( baseObj.bases[0].numberHeavyCannon == (int16_t) 1);
        REQUIRE( baseObj.bases[0].numberHCArmourPiercing == (int16_t) 6);
        REQUIRE( baseObj.bases[0].numberHCHighExplosive == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberHCIncendiary == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberAutoCanon == (int16_t) 1);
        REQUIRE( baseObj.bases[0].numberACArmourPiercing == (int16_t) 6);
        REQUIRE( baseObj.bases[0].numberACHighExplosive == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberACIncendiary == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberRocketLauncher == (int16_t) 1);
        REQUIRE( baseObj.bases[0].numberSmallRocket == (int16_t) 4);
        REQUIRE( baseObj.bases[0].numberLargeRocket == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberIncendiaryRocket == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberLaserPistol == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberLaserRifle == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberHeavyLaser == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberGrenade == (int16_t) 5);
        REQUIRE( baseObj.bases[0].numberSmokeGrenade == (int16_t) 5);
        REQUIRE( baseObj.bases[0].numberProximityGrenade == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberHighExplosive == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberMotionScanner == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberMediKit == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberPsiAmp == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberStunRod == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberElectroFlare == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberHeavyPlasma == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberHeavyPlasmaClip == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberPlasmaRifle == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberPlasmaRifleClip == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberPlasmaPistol == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberPlasmaPistolClip == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberBlasterLauncher == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberBlasterBomb == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberSmallLauncher == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberStunBomb == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberAlienGrenade == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberElerium115 == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberMindProbe == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberSectoidCorpse == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberSnakemanCorpse == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberEtherealCorpse == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberMutonCorpse == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberFloaterCorpse == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberCelatidCorpse == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberSilacoidCorpse == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberChryssalidCorpse == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberReaperCorpse == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberSectopodCorpse == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberCyberdiscCorpse == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberUfoPowerSource == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberUfoNavigation == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberUfoConstruction == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberAlienFood == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberAlienReproduction == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberAlienEntertainment == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberAlienSurgery == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberExaminationRoom == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberAlienAlloys == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberAlienHabitat == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberPersonalArmour == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberPowerSuit == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberFlyingSuit == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberHwpCannonShell == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberHwpRocket == (int16_t) 0);
        REQUIRE( baseObj.bases[0].numberHwpFusionBomb == (int16_t) 0);

        REQUIRE( baseObj.bases[0].baseActive == (int32_t) 0);
        REQUIRE( baseObj.bases[0].isBaseActive());
        REQUIRE( baseObj.bases[1].baseActive == (int32_t) 1);
        REQUIRE( !baseObj.bases[1].isBaseActive());
        REQUIRE( baseObj.bases[2].baseActive == (int32_t) 1);
        REQUIRE( !baseObj.bases[2].isBaseActive());
        REQUIRE( baseObj.bases[3].baseActive == (int32_t) 1);
        REQUIRE( !baseObj.bases[3].isBaseActive());
        REQUIRE( baseObj.bases[4].baseActive == (int32_t) 1);
        REQUIRE( !baseObj.bases[4].isBaseActive());
        REQUIRE( baseObj.bases[5].baseActive == (int32_t) 1);
        REQUIRE( !baseObj.bases[5].isBaseActive());
        REQUIRE( baseObj.bases[6].baseActive == (int32_t) 1);
        REQUIRE( !baseObj.bases[6].isBaseActive());
        REQUIRE( baseObj.bases[7].baseActive == (int32_t) 1);
        REQUIRE( !baseObj.bases[7].isBaseActive());
    }

    SECTION( "1 write game data unchanged" ) {
        {
            std::ofstream test1_1_output(TMP_TEST1_1_BASE_FILE.c_str());
            baseObj.writeToStream(test1_1_output);
        }
        compareFiles(GAME_0001_BASE_FILE, TMP_TEST1_1_BASE_FILE);
    }
}

TEST_CASE( "Test 2. Invalid base name" ) {
    Base baseObj;
    {
        std::ifstream baseFile(T2_INVALID_BASENAME_BASE_FILE.c_str());
        REQUIRE( baseFile.is_open() );
        baseObj.readFromStream(baseFile);
        REQUIRE( baseFile.peek() == EOF );
        REQUIRE( baseObj.bases[0].isBaseNameValid() == false);
        REQUIRE_THROWS_AS( baseObj.bases[0].getBaseName(), InvalidBaseDataException);
        try {
            baseObj.bases[0].getBaseName();
        } catch(const InvalidBaseDataException& e) {
            REQUIRE(e.errType == BaseDataError::UNTERMINATED_BASE_NAME);
            REQUIRE(std::string(e.what()) == InvalidBaseDataException::UNTERMINATED_BASE_NAME_MSG);
        }
    }
}

TEST_CASE( "Test 3. baseActive behaviour" ) {
    BaseData baseDataObj;
    baseDataObj.baseActive = 0;
    REQUIRE( baseDataObj.baseActive == 0 );
    REQUIRE( baseDataObj.isBaseActive() );
    baseDataObj.baseActive = 1;
    REQUIRE( baseDataObj.baseActive == 1 );
    REQUIRE( !baseDataObj.isBaseActive() );
    baseDataObj.setBaseActive(true);
    REQUIRE( baseDataObj.baseActive == 0 );
    REQUIRE( baseDataObj.isBaseActive() );
    baseDataObj.setBaseActive(false);
    REQUIRE( baseDataObj.baseActive == 1 );
    REQUIRE( !baseDataObj.isBaseActive() );
}
