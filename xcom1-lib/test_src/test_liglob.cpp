#include <catch.hpp>
#include <iostream>
#include <fstream>
#include <cstring>

#include "liglob.hpp"
#include "test_utils.hpp"
#include "test_constants.hpp"

TEST_CASE( "Test 1. Read/Write GAME_0001" ) {
    Liglob liglobObj;
    {
        std::ifstream game001Liglob(GAME_0001_LIGLOB_FILE.c_str());
        REQUIRE( game001Liglob.is_open() );
        liglobObj.readFromStream(game001Liglob);
        REQUIRE( game001Liglob.peek() == EOF );
        REQUIRE( liglobObj.money == 4153000 );
    }

    SECTION( "1 write game data unchanged" ) {
        {
            std::ofstream test1_1_output(TMP_TEST1_1_LIGLOB_FILE.c_str());
            liglobObj.writeToStream(test1_1_output);
        }
        compareFiles(GAME_0001_LIGLOB_FILE, TMP_TEST1_1_LIGLOB_FILE);
    }

    SECTION( "2 write game data different money" ) {
        liglobObj.money = 6230987;
        {
            std::ofstream test1_2_output(TMP_TEST1_2_LIGLOB_FILE.c_str());
            liglobObj.writeToStream(test1_2_output);
        }
        compareFiles(TEST1_2_LIGLOB_FILE, TMP_TEST1_2_LIGLOB_FILE);
    }
}
