#include "test_constants.hpp"

//General Constants
const std::string XCOM_LIB_DIR = XCOM_LIB_DIR_DEF;
const std::string TEST_DATA_DIR = XCOM_LIB_DIR + "/test_data";
const std::string TMP_TEST_DATA_DIR = XCOM_LIB_DIR + "/tmp_test_data";

//Example Game Constants
const std::string GAME_0001_DIR = TEST_DATA_DIR + "/GAME_0001";
const std::string GAME_0001_LIGLOB_FILE = GAME_0001_DIR + "/LIGLOB.DAT";
const std::string GAME_0001_BASE_FILE = GAME_0001_DIR + "/BASE.DAT";
const std::string GAME_0001_BASE1_BASENAME = "X-COM HQ";

//test_data_types constants
const std::string TEST_DATA_TYPES_DIR = TEST_DATA_DIR + "/test_data_types";
const std::string I32_4153000_FILE = TEST_DATA_TYPES_DIR + "/i32-4153000.dat";
const std::string TEST1_1_I32_4153000_TMP_FILE = TMP_TEST_DATA_DIR + "/i32-4153000.dat";
const std::string I32_N4153000_FILE = TEST_DATA_TYPES_DIR + "/i32-N4153000.dat";
const std::string TEST2_1_I32_N4153000_TMP_FILE = TMP_TEST_DATA_DIR + "/i32-N4153000.dat";
const std::string I16_22434_FILE = TEST_DATA_TYPES_DIR + "/i16-22434.dat";
const std::string TEST3_1_I16_22434_TMP_FILE = TMP_TEST_DATA_DIR + "/i16-22434.dat";
const std::string I16_N22434_FILE = TEST_DATA_TYPES_DIR + "/i16-N22434.dat";
const std::string TEST4_1_I16_N22434_TMP_FILE = TMP_TEST_DATA_DIR + "/i16-N22434.dat";

//test_liglob constants
const std::string TEST_LIGLOB_TMP_DIR = TMP_TEST_DATA_DIR;
const std::string TEST_LIGLOB_DATA_DIR = TEST_DATA_DIR + "/test_liglob";
const std::string TMP_TEST1_1_LIGLOB_FILE = TEST_LIGLOB_TMP_DIR + "/TEST1_1_LIGLOB.DAT";
const std::string TEST1_2_LIGLOB_FILE = TEST_LIGLOB_DATA_DIR + "/T1_2_LIGLOB.DAT";
const std::string TMP_TEST1_2_LIGLOB_FILE = TEST_LIGLOB_TMP_DIR + "/TEST1_2_LIGLOB.DAT";

//test_base constants
const std::string TEST_BASE_TMP_DIR = TMP_TEST_DATA_DIR;
const std::string TEST_BASE_DATA_DIR = TEST_DATA_DIR + "/test_base";
const std::string TMP_TEST1_1_BASE_FILE = TEST_BASE_TMP_DIR + "/TEST1_1_BASE.DAT";
const std::string T2_INVALID_BASENAME_BASE_FILE = TEST_BASE_DATA_DIR + "/T2_INVALID_BASENAME_BASE.DAT";

//test_game_data constants
const std::string NOT_REAL_PATH = "/this/does/not/exist/really";
const std::string TEST_GAME_DATA_TMP_DIR = TMP_TEST_DATA_DIR + "/test_game_data";
extern const std::string TEST_GAME_DATA_T5_1_TMP_LIGLOB = TEST_GAME_DATA_TMP_DIR + "/T5_1_LIGLOB.DAT";
