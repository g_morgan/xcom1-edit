#include "liglob.hpp"

#include <fstream>

void Liglob::readFromStream(std::istream &inputStream) {
    this->money.readFromStream(inputStream);
    inputStream.read((char *) this->data.data(), this->data.size());
}

void Liglob::writeToStream(std::ostream &outputStream) {
    this->money.writeToStream(outputStream);
    outputStream.write((char *) this->data.data(), this->data.size());
}

Liglob getLiglobFromFilename(std::string liglobFilename) {
    Liglob output;
    std::ifstream liglobReadStream(liglobFilename.c_str());
    output.readFromStream(liglobReadStream);

    return output;
}

void writeLiglobToFilename(std::string liglobFilename, Liglob& liglob) {
    std::ofstream liglobWriteStream(liglobFilename.c_str());
    liglob.writeToStream(liglobWriteStream);
}
