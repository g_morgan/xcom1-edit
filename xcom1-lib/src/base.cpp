#include "base.hpp"

void BaseData::readFromStream(std::istream &inputStream) {
    inputStream.read((char *) this->baseName.data(), this->baseName.size());
    inputStream.read((char *) this->block1.data(), this->block1.size());
    inputStream.get((char &) this->numberEngineers);
    inputStream.get((char &) this->numberScientists);
    this->numberStingrayLauncher.readFromStream(inputStream);
    this->numberAvalancheLauncher.readFromStream(inputStream);
    this->numberCannon.readFromStream(inputStream);
    this->numberFusionBallLauncher.readFromStream(inputStream);
    this->numberLaserCannon.readFromStream(inputStream);
    this->numberPlasmaBeam.readFromStream(inputStream);
    this->numberStingrayMissile.readFromStream(inputStream);
    this->numberAvalancheMissile.readFromStream(inputStream);
    this->numberCannonRounds.readFromStream(inputStream);
    this->numberFusionBall.readFromStream(inputStream);
    this->numberCannonTank.readFromStream(inputStream);
    this->numberRocketTank.readFromStream(inputStream);
    this->numberLaserTank.readFromStream(inputStream);
    this->numberPlasmaHoverTank.readFromStream(inputStream);
    this->numberLauncherHoverTank.readFromStream(inputStream);
    this->numberPistol.readFromStream(inputStream);
    this->numberPistolClip.readFromStream(inputStream);
    this->numberRifle.readFromStream(inputStream);
    this->numberRifleClip.readFromStream(inputStream);
    this->numberHeavyCannon.readFromStream(inputStream);
    this->numberHCArmourPiercing.readFromStream(inputStream);
    this->numberHCHighExplosive.readFromStream(inputStream);
    this->numberHCIncendiary.readFromStream(inputStream);
    this->numberAutoCanon.readFromStream(inputStream);
    this->numberACArmourPiercing.readFromStream(inputStream);
    this->numberACHighExplosive.readFromStream(inputStream);
    this->numberACIncendiary.readFromStream(inputStream);
    this->numberRocketLauncher.readFromStream(inputStream);
    this->numberSmallRocket.readFromStream(inputStream);
    this->numberLargeRocket.readFromStream(inputStream);
    this->numberIncendiaryRocket.readFromStream(inputStream);
    this->numberLaserPistol.readFromStream(inputStream);
    this->numberLaserRifle.readFromStream(inputStream);
    this->numberHeavyLaser.readFromStream(inputStream);
    this->numberGrenade.readFromStream(inputStream);
    this->numberSmokeGrenade.readFromStream(inputStream);
    this->numberProximityGrenade.readFromStream(inputStream);
    this->numberHighExplosive.readFromStream(inputStream);
    this->numberMotionScanner.readFromStream(inputStream);
    this->numberMediKit.readFromStream(inputStream);
    this->numberPsiAmp.readFromStream(inputStream);
    this->numberStunRod.readFromStream(inputStream);
    this->numberElectroFlare.readFromStream(inputStream);
    this->empty1.readFromStream(inputStream);
    this->empty2.readFromStream(inputStream);
    this->empty3.readFromStream(inputStream);
    this->empty4.readFromStream(inputStream);
    this->empty5.readFromStream(inputStream);
    this->empty6.readFromStream(inputStream);
    this->numberHeavyPlasma.readFromStream(inputStream);
    this->numberHeavyPlasmaClip.readFromStream(inputStream);
    this->numberPlasmaRifle.readFromStream(inputStream);
    this->numberPlasmaRifleClip.readFromStream(inputStream);
    this->numberPlasmaPistol.readFromStream(inputStream);
    this->numberPlasmaPistolClip.readFromStream(inputStream);
    this->numberBlasterLauncher.readFromStream(inputStream);
    this->numberBlasterBomb.readFromStream(inputStream);
    this->numberSmallLauncher.readFromStream(inputStream);
    this->numberStunBomb.readFromStream(inputStream);
    this->numberAlienGrenade.readFromStream(inputStream);
    this->numberElerium115.readFromStream(inputStream);
    this->numberMindProbe.readFromStream(inputStream);
    this->empty7.readFromStream(inputStream);
    this->empty8.readFromStream(inputStream);
    this->empty9.readFromStream(inputStream);
    this->numberSectoidCorpse.readFromStream(inputStream);
    this->numberSnakemanCorpse.readFromStream(inputStream);
    this->numberEtherealCorpse.readFromStream(inputStream);
    this->numberMutonCorpse.readFromStream(inputStream);
    this->numberFloaterCorpse.readFromStream(inputStream);
    this->numberCelatidCorpse.readFromStream(inputStream);
    this->numberSilacoidCorpse.readFromStream(inputStream);
    this->numberChryssalidCorpse.readFromStream(inputStream);
    this->numberReaperCorpse.readFromStream(inputStream);
    this->numberSectopodCorpse.readFromStream(inputStream);
    this->numberCyberdiscCorpse.readFromStream(inputStream);
    this->empty10.readFromStream(inputStream);
    this->empty11.readFromStream(inputStream);
    this->empty12.readFromStream(inputStream);
    this->empty13.readFromStream(inputStream);
    this->numberUfoPowerSource.readFromStream(inputStream);
    this->numberUfoNavigation.readFromStream(inputStream);
    this->numberUfoConstruction.readFromStream(inputStream);
    this->numberAlienFood.readFromStream(inputStream);
    this->numberAlienReproduction.readFromStream(inputStream);
    this->numberAlienEntertainment.readFromStream(inputStream);
    this->numberAlienSurgery.readFromStream(inputStream);
    this->numberExaminationRoom.readFromStream(inputStream);
    this->numberAlienAlloys.readFromStream(inputStream);
    this->numberAlienHabitat.readFromStream(inputStream);
    this->numberPersonalArmour.readFromStream(inputStream);
    this->numberPowerSuit.readFromStream(inputStream);
    this->numberFlyingSuit.readFromStream(inputStream);
    this->numberHwpCannonShell.readFromStream(inputStream);
    this->numberHwpRocket.readFromStream(inputStream);
    this->numberHwpFusionBomb.readFromStream(inputStream);
    this->baseActive.readFromStream(inputStream);
}

void BaseData::writeToStream(std::ostream &outputStream) {
    outputStream.write((char *) this->baseName.data(), this->baseName.size());
    outputStream.write((char *) this->block1.data(), this->block1.size());
    outputStream.put((char) this->numberEngineers);
    outputStream.put((char) this->numberScientists);
    this->numberStingrayLauncher.writeToStream(outputStream);
    this->numberAvalancheLauncher.writeToStream(outputStream);
    this->numberCannon.writeToStream(outputStream);
    this->numberFusionBallLauncher.writeToStream(outputStream);
    this->numberLaserCannon.writeToStream(outputStream);
    this->numberPlasmaBeam.writeToStream(outputStream);
    this->numberStingrayMissile.writeToStream(outputStream);
    this->numberAvalancheMissile.writeToStream(outputStream);
    this->numberCannonRounds.writeToStream(outputStream);
    this->numberFusionBall.writeToStream(outputStream);
    this->numberCannonTank.writeToStream(outputStream);
    this->numberRocketTank.writeToStream(outputStream);
    this->numberLaserTank.writeToStream(outputStream);
    this->numberPlasmaHoverTank.writeToStream(outputStream);
    this->numberLauncherHoverTank.writeToStream(outputStream);
    this->numberPistol.writeToStream(outputStream);
    this->numberPistolClip.writeToStream(outputStream);
    this->numberRifle.writeToStream(outputStream);
    this->numberRifleClip.writeToStream(outputStream);
    this->numberHeavyCannon.writeToStream(outputStream);
    this->numberHCArmourPiercing.writeToStream(outputStream);
    this->numberHCHighExplosive.writeToStream(outputStream);
    this->numberHCIncendiary.writeToStream(outputStream);
    this->numberAutoCanon.writeToStream(outputStream);
    this->numberACArmourPiercing.writeToStream(outputStream);
    this->numberACHighExplosive.writeToStream(outputStream);
    this->numberACIncendiary.writeToStream(outputStream);
    this->numberRocketLauncher.writeToStream(outputStream);
    this->numberSmallRocket.writeToStream(outputStream);
    this->numberLargeRocket.writeToStream(outputStream);
    this->numberIncendiaryRocket.writeToStream(outputStream);
    this->numberLaserPistol.writeToStream(outputStream);
    this->numberLaserRifle.writeToStream(outputStream);
    this->numberHeavyLaser.writeToStream(outputStream);
    this->numberGrenade.writeToStream(outputStream);
    this->numberSmokeGrenade.writeToStream(outputStream);
    this->numberProximityGrenade.writeToStream(outputStream);
    this->numberHighExplosive.writeToStream(outputStream);
    this->numberMotionScanner.writeToStream(outputStream);
    this->numberMediKit.writeToStream(outputStream);
    this->numberPsiAmp.writeToStream(outputStream);
    this->numberStunRod.writeToStream(outputStream);
    this->numberElectroFlare.writeToStream(outputStream);
    this->empty1.writeToStream(outputStream);
    this->empty2.writeToStream(outputStream);
    this->empty3.writeToStream(outputStream);
    this->empty4.writeToStream(outputStream);
    this->empty5.writeToStream(outputStream);
    this->empty6.writeToStream(outputStream);
    this->numberHeavyPlasma.writeToStream(outputStream);
    this->numberHeavyPlasmaClip.writeToStream(outputStream);
    this->numberPlasmaRifle.writeToStream(outputStream);
    this->numberPlasmaRifleClip.writeToStream(outputStream);
    this->numberPlasmaPistol.writeToStream(outputStream);
    this->numberPlasmaPistolClip.writeToStream(outputStream);
    this->numberBlasterLauncher.writeToStream(outputStream);
    this->numberBlasterBomb.writeToStream(outputStream);
    this->numberSmallLauncher.writeToStream(outputStream);
    this->numberStunBomb.writeToStream(outputStream);
    this->numberAlienGrenade.writeToStream(outputStream);
    this->numberElerium115.writeToStream(outputStream);
    this->numberMindProbe.writeToStream(outputStream);
    this->empty7.writeToStream(outputStream);
    this->empty8.writeToStream(outputStream);
    this->empty9.writeToStream(outputStream);
    this->numberSectoidCorpse.writeToStream(outputStream);
    this->numberSnakemanCorpse.writeToStream(outputStream);
    this->numberEtherealCorpse.writeToStream(outputStream);
    this->numberMutonCorpse.writeToStream(outputStream);
    this->numberFloaterCorpse.writeToStream(outputStream);
    this->numberCelatidCorpse.writeToStream(outputStream);
    this->numberSilacoidCorpse.writeToStream(outputStream);
    this->numberChryssalidCorpse.writeToStream(outputStream);
    this->numberReaperCorpse.writeToStream(outputStream);
    this->numberSectopodCorpse.writeToStream(outputStream);
    this->numberCyberdiscCorpse.writeToStream(outputStream);
    this->empty10.writeToStream(outputStream);
    this->empty11.writeToStream(outputStream);
    this->empty12.writeToStream(outputStream);
    this->empty13.writeToStream(outputStream);
    this->numberUfoPowerSource.writeToStream(outputStream);
    this->numberUfoNavigation.writeToStream(outputStream);
    this->numberUfoConstruction.writeToStream(outputStream);
    this->numberAlienFood.writeToStream(outputStream);
    this->numberAlienReproduction.writeToStream(outputStream);
    this->numberAlienEntertainment.writeToStream(outputStream);
    this->numberAlienSurgery.writeToStream(outputStream);
    this->numberExaminationRoom.writeToStream(outputStream);
    this->numberAlienAlloys.writeToStream(outputStream);
    this->numberAlienHabitat.writeToStream(outputStream);
    this->numberPersonalArmour.writeToStream(outputStream);
    this->numberPowerSuit.writeToStream(outputStream);
    this->numberFlyingSuit.writeToStream(outputStream);
    this->numberHwpCannonShell.writeToStream(outputStream);
    this->numberHwpRocket.writeToStream(outputStream);
    this->numberHwpFusionBomb.writeToStream(outputStream);
    this->baseActive.writeToStream(outputStream);
}

bool BaseData::isBaseNameValid() {
    for(char curChar : this->baseName) {
        if('\0' == curChar) {
            return true;
        }
    }
    return false;
}

std::string BaseData::getBaseName() {
    if(this->isBaseNameValid()) {
        std::string output(this->baseName.data());
        return output;
    } else {
        throw InvalidBaseDataException(InvalidBaseDataException::UNTERMINATED_BASE_NAME_MSG, BaseDataError::UNTERMINATED_BASE_NAME);
    }
}

bool BaseData::isBaseActive() {
    return this->baseActive == 0;
}

void BaseData::setBaseActive(bool activeValue) {
    if(activeValue) {
        this->baseActive = 0;
    } else {
        this->baseActive = 1;
    }
}

void Base::readFromStream(std::istream &inputStream) {
    for(BaseData& curBase : this->bases) {
        curBase.readFromStream(inputStream);
    }
}

void Base::writeToStream(std::ostream &outputStream) {
    for(BaseData& curBase : this->bases) {
        curBase.writeToStream(outputStream);
    }
}

const std::string InvalidBaseDataException::UNTERMINATED_BASE_NAME_MSG = "Unterminated base name";

const char* InvalidBaseDataException::what() const noexcept{
    return this->errMessage;
}