#include "game_data.hpp"

#include <fstream>
#include <array>

#include <boost/filesystem.hpp>

const std::string ACTS_DAT = "ACTS.DAT";
const std::string AKNOW_DAT = "AKNOW.DAT";
const std::string ALIEN_DAT = "ALIEN.DAT";
const std::string ASTORE_DAT = "ASTORE.DAT";
const std::string BASE_DAT = "BASE.DAT";
const std::string BPROD_DAT = "BPROD.DAT";
const std::string CRAFT_DAT = "CRAFT.DAT";
const std::string DIPLOM_DAT = "DIPLOM.DAT";
const std::string FACIL_DAT = "FACIL.DAT";
const std::string IGLOB_DAT = "IGLOB.DAT";
const std::string INTER_DAT = "INTER.DAT";
const std::string LEASE_DAT = "LEASE.DAT";
const std::string LIGLOB_DAT = "LIGLOB.DAT";
const std::string LOC_DAT = "LOC.DAT";
const std::string MISSIONS_DAT = "MISSIONS.DAT";
const std::string PRODUCT_DAT = "PRODUCT.DAT";
const std::string PROJECT_DAT = "PROJECT.DAT";
const std::string PURCHASE_DAT = "PURCHASE.DAT";
const std::string RESEARCH_DAT = "RESEARCH.DAT";
const std::string SAVEINFO_DAT = "SAVEINFO.DAT";
const std::string SOLDIER_DAT = "SOLDIER.DAT";
const std::string TRANSFER_DAT = "TRANSFER.DAT";
const std::string UIGLOB_DAT = "UIGLOB.DAT";
const std::string UP_DAT = "UP.DAT";
const std::string XBASES_DAT = "XBASES.DAT";
const std::string XCOM_DAT = "XCOM.DAT";
const std::string ZONAL_DAT = "ZONAL.DAT";

const std::array<std::string, 27> saveGameFilenames = {
    ACTS_DAT,
    AKNOW_DAT,
    ALIEN_DAT,
    ASTORE_DAT,
    BASE_DAT,
    BPROD_DAT,
    CRAFT_DAT,
    DIPLOM_DAT,
    FACIL_DAT,
    IGLOB_DAT,
    INTER_DAT,
    LEASE_DAT,
    LIGLOB_DAT,
    LOC_DAT,
    MISSIONS_DAT,
    PRODUCT_DAT,
    PROJECT_DAT,
    PURCHASE_DAT,
    RESEARCH_DAT,
    SAVEINFO_DAT,
    SOLDIER_DAT,
    TRANSFER_DAT,
    UIGLOB_DAT,
    UP_DAT,
    XBASES_DAT,
    XCOM_DAT,
    ZONAL_DAT
};

bool containsGameDataFiles(std::string saveGameDir) {
    for(std::string curGameFilename : saveGameFilenames) {
        std::string curGameFile = saveGameDir + "/" + curGameFilename;
        boost::filesystem::path curGameFilePath(curGameFile);

        if(!boost::filesystem::exists(curGameFilePath)) {
            return false;
        }

        if(!boost::filesystem::is_regular_file(curGameFilePath)) {
            return false;
        }
    }

    return true;
}

bool isValidSaveGameDirectory(std::string saveGameDir) {
    boost::filesystem::path saveGamePath(saveGameDir);

    if(!boost::filesystem::exists(saveGamePath)) {
        return false;
    }

    if(!boost::filesystem::is_directory(saveGamePath)) {
        return false;
    }

    return containsGameDataFiles(saveGameDir);
}

const std::string InvalidSaveGameDirectoryException::INVALID_GAME_DIR_MSG = "Invalid save game directory: ";

GameData::GameData(const std::string& saveDir) : saveDir(saveDir) {
    if(!isValidSaveGameDirectory(this->saveDir)) {
        throw InvalidSaveGameDirectoryException(this->saveDir);
    }
}

std::string GameData::getLiglobFilename() {
    return this->saveDir + "/" + LIGLOB_DAT;
}

Liglob GameData::getLiglob() {
    return getLiglobFromFilename(this->getLiglobFilename());
}

void GameData::writeLiglob(Liglob& liglob) {
    writeLiglobToFilename(this->getLiglobFilename(), liglob);
}
